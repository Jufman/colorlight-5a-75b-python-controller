from colorlight import Colorlight
from PIL import Image

# Initialize the Colorlight instance for the given interface name
cl = Colorlight('eth0')

# Set the brightness of the screen
cl.send_brightness(0x80, 0x80, 0x80)  # Moderate brightness

# Load and resize the image
image = Image.open('image.png').resize((128, 64))

# Ensure the image is in RGB mode
image = image.convert('RGB')

# Convert the image data to a list of BGR values
pixel_data = bytearray()
for y in range(64):
    for x in range(128):
        r, g, b = image.getpixel((x, y))
        pixel_data.extend([b, g, r])  # Convert to BGR

# Send pixel data for each row of the screen
for row in range(64):
    row_data = pixel_data[row * 128 * 3:(row + 1) * 128 * 3]
    cl.send_row(row, 128, row_data)

# Update the screen with the new image
cl.send_update(0x00, 0x00, 0x00)

# Close the socket after operations
cl.close()
