# LED Panel NDI Stream Setup

https://github.com/buresu/ndi-python

## Prerequisites

1. **Python 3.10**: Ensure you have Python 3.10 installed.

    ```bash
    sudo apt install python3.10 python3.10-venv python3.10-dev
    ```

2. **Avahi Daemon**: Install and enable the Avahi daemon for network service discovery.

    ```bash
    sudo apt install avahi-daemon
    sudo systemctl enable --now avahi-daemon
    ```

3. **Avahi Client Library**: Install the Avahi client library.

    ```bash
    sudo apt-get install libavahi-client3
    ```

## NDI Tools

Install NDI tools if they are needed for your setup (not mandatory for basic functionality).

## Python Dependencies

Use `pip` to install the necessary Python packages:

1. **Pillow**: For image processing.
2. **NDI-Python**: For NDI stream handling.

```bash
pip install pillow ndi-python
