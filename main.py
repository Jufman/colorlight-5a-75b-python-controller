# websocket_server.py

import asyncio
import websockets
import json
from colorlight import Colorlight

# Define screen resolution
SCREEN_WIDTH = 128
SCREEN_HEIGHT = 64

cl = Colorlight('eth0')
cl.send_brightness(0x80, 0x80, 0x80)  # Set moderate brightness

async def update_display(websocket, path):
    try:
        async for message in websocket:
            print("Received message")
            data = json.loads(message)
            if data['type'] == 'update':
                print("Processing update")
                image_data = data['image']
                pixel_data = bytearray(image_data)

                # Send pixel data for each row of the screen
                for row in range(SCREEN_HEIGHT):
                    row_data = pixel_data[row * SCREEN_WIDTH * 3:(row + 1) * SCREEN_WIDTH * 3]
                    cl.send_row(row, SCREEN_WIDTH, row_data)

                # Update the screen with the new image
                cl.send_update(0x00, 0x00, 0x00)
                print("Updated display")
    except websockets.ConnectionClosed:
        print("Client disconnected")
    except Exception as e:
        print(f"An error occurred: {e}")
    finally:
        print("Cleaning up after client")

start_server = websockets.serve(update_display, 'led-pi.local', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
