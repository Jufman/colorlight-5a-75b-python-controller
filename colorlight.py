# colorlight.py

import socket
import struct
import fcntl

MAX_PIXELS = 497
ETH_P_ALL = 0x0003  # Manually define ETH_P_ALL

class Colorlight:
    def __init__(self, interface):
        self.socket = None
        self.interface = interface
        self.dst_mac = b'\x11\x22\x33\x44\x55\x66'
        self.src_mac = b'\x22\x22\x33\x44\x55\x66'
        self.ethertype = b'\x55'  # For image data packets

        self.init_socket()

    def init_socket(self):
        self.socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_ALL))

        # Get interface index
        ifr = struct.pack('16sH14s', self.interface.encode('utf-8'), socket.AF_PACKET, b'\x00' * 14)
        try:
            result = fcntl.ioctl(self.socket.fileno(), 0x8933, ifr)
        except IOError as e:
            raise RuntimeError(f"Failed to find interface: {e}")

        ifindex = struct.unpack('16sH14s', result)[1]
        self.socket.bind((self.interface, 0))

    def send_row(self, row, width, data):
        for offset in range(0, width, MAX_PIXELS):
            pixel_count = min(width - offset, MAX_PIXELS)
            header = struct.pack('!HHHBB', row, offset, pixel_count, 0x08, 0x88)

            packet_data = self.dst_mac + self.src_mac + self.ethertype + header + data[offset * 3:(offset + pixel_count) * 3]
            try:
                self.socket.send(packet_data)
            except OSError as e:
                print(f"Failed to send row data packet: {e}")

    def send_update(self, red, green, blue):
        packet = bytearray(100)
        packet[0] = 0x07  # Command for display frame
        packet[22] = 0x05  # Constant value
        packet[24] = red
        packet[25] = green
        packet[26] = blue

        # Ensure the rest of the packet is zeroed out
        for i in range(27, 100):
            packet[i] = 0x00

        packet_data = self.dst_mac + self.src_mac + b'\x01' + packet

        try:
            self.socket.send(packet_data)
        except OSError as e:
            print(f"Failed to send update packet: {e}")

    def send_brightness(self, red, green, blue):
        packet = bytearray(65)
        packet[0] = blue   # BGR order
        packet[1] = green
        packet[2] = red    # BGR order
        packet[3] = 0xFF  # Alpha value

        for i in range(4, 65):
            packet[i] = 0x00

        packet_data = self.dst_mac + self.src_mac + b'\x0A' + packet

        try:
            self.socket.send(packet_data)
        except OSError as e:
            print(f"Failed to send brightness packet: {e}")

    def close(self):
        self.socket.close()
