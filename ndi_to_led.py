import os
import time
import numpy as np
from PIL import Image
from colorlight import Colorlight
import NDIlib as ndi

# Read configuration from environment variables with defaults
SCREEN_WIDTH = int(os.getenv('SCREEN_WIDTH', 128))
SCREEN_HEIGHT = int(os.getenv('SCREEN_HEIGHT', 64))
INTERFACE_NAME = os.getenv('INTERFACE_NAME', 'eth0')
NDI_SOURCE_IP_LIST = os.getenv('NDI_SOURCE_IP_LIST', '192.168.4.32')
SOURCE_INDEX = int(os.getenv('SOURCE_INDEX', 0))

# Function to scale the image to fit the screen resolution
def scale_image(image, target_width, target_height):
    return image.resize((target_width, target_height), Image.LANCZOS)

def main():
    # Initialize the Colorlight instance for the given interface name
    cl = Colorlight(INTERFACE_NAME)
    cl.send_brightness(0x80, 0x80, 0x80)  # Set moderate brightness

    if not ndi.initialize():
        print("Cannot initialize NDI.")
        return 0

    NDI_find_create_desc = ndi.FindCreate()
    NDI_find_create_desc.extra_ips = NDI_SOURCE_IP_LIST
    ndi_find = ndi.find_create_v2(NDI_find_create_desc)
    if ndi_find is None:
        print("Cannot create NDI finder.")
        return 0

    sources = []
    while not len(sources) > 0:
        print('Looking for sources ...')
        ndi.find_wait_for_sources(ndi_find, 1000)
        sources = ndi.find_get_current_sources(ndi_find)

    print("Available NDI Sources:")
    for i, source in enumerate(sources):
        print(f"{i}: {source.ndi_name}")

    source = sources[SOURCE_INDEX]

    ndi_recv_create = ndi.RecvCreateV3()
    ndi_recv_create.color_format = ndi.RECV_COLOR_FORMAT_BGRX_BGRA

    ndi_recv = ndi.recv_create_v3(ndi_recv_create)
    if ndi_recv is None:
        print("Cannot create NDI receiver.")
        return 0

    ndi.recv_connect(ndi_recv, source)

    ndi.find_destroy(ndi_find)

    try:
        while True:
            # Receive a frame from the NDI stream
            t, v, a, _ = ndi.recv_capture_v2(ndi_recv, 5000)

            if t != ndi.FRAME_TYPE_VIDEO:
                continue

            frame = np.ctypeslib.as_array(v.data, (v.yres, v.xres, 4))
            frame = np.copy(frame)  # Ensure we have a contiguous array

            ndi.recv_free_video_v2(ndi_recv, v)

            # Convert the frame to an image
            image = Image.fromarray(frame[:, :, :3].astype('uint8'), 'RGB')

            # Scale the image to fit the screen resolution
            image = scale_image(image, SCREEN_WIDTH, SCREEN_HEIGHT)

            # Convert the image to RGB format
            image = image.convert('RGB')

            # Extract the pixel data from the image
            pixel_data = bytearray()
            for y in range(SCREEN_HEIGHT):
                for x in range(SCREEN_WIDTH):
                    r, g, b = image.getpixel((x, y))
                    pixel_data.extend([r, g, b])  # Convert to BGR

            # Send the pixel data to the LED panel
            for row in range(SCREEN_HEIGHT):
                row_data = pixel_data[row * SCREEN_WIDTH * 3:(row + 1) * SCREEN_WIDTH * 3]
                cl.send_row(row, SCREEN_WIDTH, row_data)

            # Update the screen with the new image
            cl.send_update(0x00, 0x00, 0x00)

            # Sleep for a short time to limit the frame rate
            time.sleep(1 / 30)  # Adjust the frame rate as needed

    finally:
        ndi.recv_destroy(ndi_recv)
        ndi.destroy()
        cl.close()
        print("Crashed Out! BYE!")
        main()

if __name__ == '__main__':
    main()
